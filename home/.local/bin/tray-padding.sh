#!/bin/bash
# Copied from https://github.com/jaor/xmobar/issues/239#issuecomment-233206552
# Detects the width of running trayer-srg window (xprop name 'panel')
# and creates an XPM icon of that width, 1px height, and transparent.
# Outputs an <icon>-tag for use in xmobar to display the generated
# XPM icon.
#
# Run script from xmobar:
# `Run Com "/where/ever/trayer-padding-icon.sh" [] "trayerpad" 10`
# and use `%trayerpad%` in your template.

# Function to create a transparent Wx1 px XPM icon
printSpaces() {
    spaces=1
    total=$1

    while [ $total -gt 15 ]; do
        spaces=$((spaces + 1))
        total=$((total - 15))
    done
    for i in $(seq $spaces); do echo -n " "; done
    echo -n "   "
}

# Width of the trayer window
width=$(xprop -name panel | grep 'program specified minimum size' | cut -d ' ' -f 5)

printSpaces $width
# Output the icon tag for xmobar
# echo "<icon=${iconfile}/>"
