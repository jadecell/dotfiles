#!/bin/sh

run() {
    ! pidof "$1" > /dev/null 2>&1 && "$@" &
}

# set a delay to allow river to launch
sleep 1s && LAUNCH=1

if [ "$LAUNCH" -eq 1 ]; then
    # Sets screens
    wlr-randr --output DP-1 --preferred --on --mode 1920x1080@239.964005 --pos 1920,0 --output DP-2 --on --mode 1920x1080@119.982002 --pos 0,0

    # Waybar
    run waybar

    # Automount USBs
    run udiskie

    # Notifications
    run fnott

    # mpd
    run mpd

    # wallpaper
    wbg ~/.config/wallpaper &

    # Polkit
    run polkit-dumb-agent

fi
