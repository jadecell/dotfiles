--[[
lvim is the global options object

Linters should be
filled in as strings with either
a global executable or a path to
an executable
]] -- THESE ARE EXAMPLE CONFIGS FEEL FREE TO CHANGE TO WHATEVER YOU WANT
-- general
lvim.log.level = "warn"
lvim.format_on_save = true
-- local default_colors = require( "kanagawa.colors" ).setup()

-- -- this will affect all the hl-groups where the redefined colors are used
-- local my_colors = { sumiInk1 = "#16161d" }

-- require"kanagawa".setup( { colors = my_colors } )
-- vim.g.tokyonight_style = "night"
-- lvim.colorscheme = "tokyonight"

vim.g.vscode_style = "dark"
lvim.colorscheme = "vscode"

-- vim.g.material_style = "darker"
-- lvim.colorscheme = "material"

-- keymappings [view all the defaults by pressing <leader>Lk]
lvim.leader = "space"
-- add your own keymapping
-- lvim.keys.normal_mode["<C-s>"] = ":w<cr>" unmap a default keymapping
-- lvim.keys.normal_mode["<C-Up>"] = false
-- edit a default keymapping
-- lvim.keys.normal_mode["<C-q>"] = ":q<cr>"

-- Change Telescope navigation to use j and k for navigation and n and p for history in both input and normal mode.
-- we use protected-mode (pcall) just in case the plugin wasn't loaded yet.
-- local _, actions = pcall(require, "telescope.actions")
-- lvim.builtin.telescope.defaults.mappings = {
--   -- for input mode
--   i = {
--     ["<C-j>"] = actions.move_selection_next,
--     ["<C-k>"] = actions.move_selection_previous,
--     ["<C-n>"] = actions.cycle_history_next,
--     ["<C-p>"] = actions.cycle_history_prev,
--   },
--   -- for normal mode
--   n = {
--     ["<C-j>"] = actions.move_selection_next,
--     ["<C-k>"] = actions.move_selection_previous,
--   },
-- }

-- Use which-key to add extra bindings with the leader-key prefix
-- lvim.builtin.which_key.mappings["P"] = { "<cmd>Telescope projects<CR>", "Projects" }
lvim.builtin.which_key.mappings["t"] = {
    name = "+Trouble",
    r = { "<cmd>Trouble lsp_references<cr>", "References" },
    f = { "<cmd>Trouble lsp_definitions<cr>", "Definitions" },
    d = { "<cmd>Trouble lsp_document_diagnostics<cr>", "Diagnostics" },
    q = { "<cmd>Trouble quickfix<cr>", "QuickFix" },
    l = { "<cmd>Trouble loclist<cr>", "LocationList" },
    w = { "<cmd>Trouble lsp_workspace_diagnostics<cr>", "Diagnostics" },
    t = { "<cmd>TroubleToggle<cr>", "Toggle Trouble Window" }
}
lvim.builtin.which_key.mappings["r"] = {
    "<cmd>silent !compiler %<CR>", "compiler"
}

-- TODO: User Config for predefined plugins
-- After changing plugin config exit and reopen LunarVim, Run :PackerInstall :PackerCompile
-- lvim.builtin.dashboard.active = true
lvim.builtin.notify.active = false
lvim.builtin.terminal.active = true
lvim.builtin.nvimtree.setup.view.side = "left"
lvim.builtin.nvimtree.show_icons.git = 0

-- if you don't want all the parsers change this to a table of the ones you want
lvim.builtin.treesitter.ensure_installed = {
    "bash", "c", "javascript", "json", "lua", "python", "typescript", "tsx",
    "css", "rust", "java", "yaml"
}

lvim.builtin.treesitter.ignore_install = { "haskell" }
lvim.builtin.treesitter.highlight.enabled = true

-- Status line
-- local colors = {
--     none = "NONE",
--     bg_dark = "#1f2335",
--     bg = "#1a1b26",
--     bg_highlight = "#292e42",
--     terminal_black = "#414868",
--     fg = "#c0caf5",
--     fg_dark = "#a9b1d6",
--     fg_gutter = "#3b4261",
--     dark3 = "#545c7e",
--     comment = "#565f89",
--     dark5 = "#737aa2",
--     blue0 = "#3d59a1",
--     blue = "#7aa2f7",
--     cyan = "#7dcfff",
--     blue1 = "#2ac3de",
--     blue2 = "#0db9d7",
--     blue5 = "#89ddff",
--     blue6 = "#B4F9F8",
--     blue7 = "#394b70",
--     magenta = "#bb9af7",
--     magenta2 = "#ff007c",
--     purple = "#9d7cd8",
--     orange = "#ff9e64",
--     yellow = "#e0af68",
--     green = "#9ece6a",
--     green1 = "#73daca",
--     green2 = "#41a6b5",
--     teal = "#1abc9c",
--     red = "#f7768e",
--     red1 = "#db4b4b",
--     git = {
--         change = "#6183bb",
--         add = "#449dab",
--         delete = "#914c54",
--         conflict = "#bb7a61"
--     },
--     gitSigns = { add = "#164846", change = "#394b70", delete = "#823c41" }
-- }

-- lvim.builtin.lualine.options.theme = {
--     normal = {
--         c = { fg = colors.fg, bg = colors.bg_dark },
--         a = { fg = colors.bg_dark, bg = colors.orange, gui = "bold" },
--         b = { fg = colors.orange, bg = colors.bg_dark }
--     },
--     insert = {
--         a = { fg = colors.fg, bg = colors.teal, gui = "bold" },
--         b = { fg = colors.teal, bg = colors.bg_dark }
--     },
--     visual = {
--         a = { fg = colors.fg, bg = colors.blue, gui = "bold" },
--         b = { fg = colors.blue, bg = colors.bg_dark }
--     },
--     replace = {
--         a = { fg = colors.fg, bg = colors.red, gui = "bold" },
--         b = { fg = colors.red, bg = colors.bg_dark }
--     },
--     inactive = {
--         c = { fg = colors.orange, bg = colors.bg_dark },
--         a = { fg = colors.bg_dark, bg = colors.teal, gui = "bold" },
--         b = { fg = colors.yellow, bg = colors.bg_dark }
--     }
-- }

-- generic LSP settings

-- ---@usage disable automatic installation of servers
-- lvim.lsp.automatic_servers_installation = false

-- ---@usage Select which servers should be configured manually. Requires `:LvimCacheRest` to take effect.
-- See the full default list `:lua print(vim.inspect(lvim.lsp.override))`
-- vim.list_extend(lvim.lsp.override, { "pyright" })

-- ---@usage setup a server -- see: https://www.lunarvim.org/languages/#overriding-the-default-configuration
-- local opts = {} -- check the lspconfig documentation for a list of all possible options
-- require("lvim.lsp.manager").setup("pylsp", opts)

-- -- you can set a custom on_attach function that will be used for all the language servers
-- -- See <https://github.com/neovim/nvim-lspconfig#keybindings-and-completion>
-- lvim.lsp.on_attach_callback = function(client, bufnr)
--   local function buf_set_option(...)
--     vim.api.nvim_buf_set_option(bufnr, ...)
--   end
--   --Enable completion triggered by <c-x><c-o>
--   buf_set_option("omnifunc", "v:lua.vim.lsp.omnifunc")
-- end

-- -- set a formatter, this will override the language server formatting capabilities (if it exists)
local formatters = require "lvim.lsp.null-ls.formatters"
formatters.setup {
    {
        command = "lua-format",
        filetypes = { "lua" },
        extra_args = {
            "--indent-width=4", "--no-use-tab", "--align-args",
            "--align-parameter", "--single-quote-to-double-quote",
            "--spaces-inside-functiondef-parens",
            "--spaces-inside-functioncall-parens",
            "--spaces-inside-table-braces", "--spaces-around-equals-in-field"
        }
    }, { command = "black", filetypes = { "python" } }, {
        command = "shfmt",
        filetypes = { "sh" },
        extra_args = { "-i", "4", "-bn", "-ci", "-sr" }
    }, {
        -- each formatter accepts a list of options identical to https://github.com/jose-elias-alvarez/null-ls.nvim/blob/main/doc/BUILTINS.md#Configuration
        command = "prettier",
        ---@usage arguments to pass to the formatter
        -- these cannot contain whitespaces, options such as `--line-width 80` become either `{'--line-width', '80'}` or `{'--line-width=80'}`
        extra_args = { "--print-width", "100" },
        ---@usage specify which filetypes to enable. By default a providers will attach to all the filetypes it supports.
        filetypes = {
            "typescript", "typescriptreact", "html", "css", "javascript"
        }
    }
}

-- -- set additional linters
local linters = require "lvim.lsp.null-ls.linters"
linters.setup {
    { command = "flake8", filetypes = { "python" } }, {
        -- each linter accepts a list of options identical to https://github.com/jose-elias-alvarez/null-ls.nvim/blob/main/doc/BUILTINS.md#Configuration
        command = "shellcheck"
    }, {
        command = "codespell",
        ---@usage specify which filetypes to enable. By default a providers will attach to all the filetypes it supports.
        filetypes = { "javascript", "python" }
    }
}

-- Additional Plugins
lvim.plugins = {
    { "norcalli/nvim-colorizer.lua" }, { "folke/trouble.nvim" },
    { "bluz71/vim-moonfly-colors" }, { "rebelot/kanagawa.nvim" },
    { "folke/tokyonight.nvim" }, { "Mofiqul/vscode.nvim" },
    { "marko-cerovac/material.nvim" },
    { "ChristianChiarulli/nvcode-color-schemes.vim" }
}

-- Autocommands (https://neovim.io/doc/user/autocmd.html)
lvim.autocommands.custom_groups = {
    { "BufEnter", "*", "ColorizerAttachToBuffer" }
}
