local null_ls = require "null-ls"
local b = null_ls.builtins

local sources = {
    b.formatting.lua_format.with {
        extra_args = {
            "--indent-width=4", "--no-use-tab", "--align-args",
            "--align-parameter", "--single-quote-to-double-quote",
            "--spaces-inside-functiondef-parens",
            "--spaces-inside-functioncall-parens",
            "--spaces-inside-table-braces", "--spaces-around-equals-in-field"
        }
    }, b.diagnostics.luacheck.with { extra_args = { "--global vim" } },

    -- Shell
    b.formatting.shfmt.with { extra_args = { "-i", "4", "-bn", "-ci", "-sr" } },
    b.diagnostics.shellcheck.with { diagnostics_format = "#{m} [#{c}]" }
}

local M = {}

M.setup = function()
    null_ls.setup {
        debug = true,
        sources = sources,

        -- format on save
        on_attach = function( client )
            if client.resolved_capabilities.document_formatting then
                vim.cmd "autocmd BufWritePre <buffer> lua vim.lsp.buf.formatting()"
            end
        end
    }
end

return M
