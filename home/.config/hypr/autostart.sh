#!/usr/bin/env sh

#gentoo-pipewire-launcher &

run() {
	! pidof "$1" >/dev/null 2>&1 && "$@" &
}

# Changes some X settings
# xset r rate 300 50
# xset s off -dpms

# Uses xrdb to set the colors
# xrdb "${XDG_CONFIG_HOME:-$HOME/.config}/x11/xresources" &

# Switch Capslock with L-CTRL for a better emacs experience
#xmodmap "${XDG_CONFIG_HOME:-$HOME/.config}/x11/xmodmap" &

# Automount USBs
run udiskie

# Fixes monitor placement and resolution
#run ~/.config/fixmonitors.sh

# Starts the statusbar
run waybar
# run dwmblocks

# Starts xcompmgr
# run xcompmgr

run mpd

# compositor
# run picom

# Sets wallpaper
run setbg

# Changes the window manager name to let Java IDEs run without complaining
# (sleep 5s && wmname "LG3D") &

# Notifications
# run dunst
run mako

#killall -q ibus-daemon
#run ibus-daemon -drxR

# Launch pipewire on gentoo
# dbus-launch --sh-syntax --exit-with-session
# pulseaudio --kill
# pipewire &

# Polkit
run polkit-dumb-agent
#killall -q polkit-gnome-authentication-agent-1
#/usr/libexec/polkit-gnome-authentication-agent-1 &

# Nextcloud
# pidof nextcloud 2>&1 || nextcloud --background &

#killall -q plex-mpv-shim
#plex-mpv-shim &

# trayer
# killall -q trayer
# trayer --edge top --align right --widthtype request --padding 6 --SetDockType true --SetPartialStrut true --expand true --monitor 0 --transparent true --alpha 0 --height 20 --tint 0x101317 --distance 8 --distancefrom right &

#~/.config/polybar/launch.sh &

# killall -q sxhkd
