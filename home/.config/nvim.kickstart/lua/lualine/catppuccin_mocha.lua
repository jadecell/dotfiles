-- stylua: ignore
local colors = {
  rosewater = "#F5E0DC",
  flamingo = "#F2CDCD",
  pink = "#F5C2E7",
  mauve = "#CBA6F7",
  red = "#F38BA8",
  maroon = "#EBA0AC",
  peach = "#FAB387",
  yellow = "#F9E2AF",
  green = "#A6E3A1",
  teal = "#94E2D5",
  sky = "#89DCEB",
  sapphire = "#74C7EC",
  blue = "#89B4FA",
  lavender = "#B4BEFE",
  text = "#CDD6F4",
  subtext1 = "#BAC2DE",
  subtext0 = "#A6ADC8",
  overlay2 = "#9399B2",
  overlay1 = "#7F849C",
  overlay0 = "#6C7086",
  surface2 = "#585B70",
  surface1 = "#45475A",
  surface0 = "#313244",
  base = "#1E1E2E",
  mantle = "#181825",
  crust = "#11111B"
}

return {
  normal = {
    a = { fg = colors.crust, bg = colors.mauve, gui = 'bold' },
    b = { fg = colors.mauve, bg = colors.overlay0 },
    c = { fg = colors.subtext1, bg = colors.mantle },
  },
  insert = {
    a = { fg = colors.crust, bg = colors.peach, gui = 'bold' },
    b = { fg = colors.peach, bg = colors.overlay0 },
  },
  visual = {
    a = { fg = colors.crust, bg = colors.green, gui = 'bold' },
    b = { fg = colors.green, bg = colors.overlay0 },
  },
  replace = {
    a = { fg = colors.crust, bg = colors.red, gui = 'bold' },
    b = { fg = colors.red, bg = colors.overlay0 },
  },
  inactive = {
    a = { fg = colors.crust, bg = colors.overlay0, gui = 'bold' },
    b = { fg = colors.crust, bg = colors.overlay0 },
    c = { fg = colors.crust, bg = colors.overlay0 },
  },
}
