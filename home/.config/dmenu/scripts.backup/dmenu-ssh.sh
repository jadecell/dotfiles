#!/usr/bin/env bash

# Source all the settings
. $HOME/.config/dmenu/settings

declare options=("quit
144.202.91.140")

choice=$(echo -e "${options[@]}" | fuzzel-in -p 'SSH to: ')

case "$choice" in
    quit)
        echo "Program terminated." && exit 1
        ;;
esac

[ -n "$choice" ] && $TERMINAL -e ssh "$(whoami)@$choice"
