#!/usr/bin/env sh

# Source all the settings
. $HOME/.config/dmenu/settings

# Dmenu binary prompt.
# Gives a dmenu-wl prompt labled with $1 to perform command $2.
# Example: dprompt "Suspend?" "sudo systemctl suspend"

[ "$(printf 'no\nyes' | dmenu-wl $DMENU_ARGUMENTS -p "$1")" = 'yes' ] && $2
