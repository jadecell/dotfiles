# Mask all overlay packages intially
*/*::guru
*/*::thegreatmcpain
*/*::src_prepare-overlay
*/*::menelkir
*/*::bobwya
*/*::tlp
*/*::crocket-overlay
*/*::nest
*/*::nitratesky
*/*::pf4public
*/*::waffle-builds
*/*::robertgzr
*/*::ricerlay
*/*::nymphos
*/*::useless-overlay
*/*::wayland-desktop
*/*::steam-overlay
*/*::cova

=sys-libs/glibc-9999
=sys-kernel/gentoo-sources-6.3.0
